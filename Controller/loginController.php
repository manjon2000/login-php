<?php

if( isset( $_SESSION['not-found-user'] ) ) {
     unset($_SESSION['not-found-user']);
}
if( isset( $_SESSION['error-password'] ) ) {
     unset($_SESSION['error-password']);
}


include __DIR__ . '/../Models/loginModel.php';

class loginController {

    public function __construct(
        public String $email,
        public String $pass
    ) {}

    public function checkUser() {
        $userCheck = new loginModel( $this->email );
        $user = $userCheck->findUser();
        
        if( $user != 'error' ) {
            return $user;
        } else {
            $_SESSION['not-found-user'] = 'No se ha encontrado el usuario';
            return 'error';
        }

    }
    public function getPassword() {
        return $this->pass;
    }
}
