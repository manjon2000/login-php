<?php

if (!$_SESSION['user']) {
    header('Location: http://localhost/login/?page=login ');
}

?>
<nav class="navbar navbar-expand-lg navbar-light bg-primary px-5 py-4">
  <a class="navbar-brand text-white font-weight-bold" href="http://localhost/login/?page=logout">LOGOTYPE</a>
</nav>

<div class="card w-75 m-auto border-light my-5 shadow">
  <div class="card-body my-4">
    <h1 class="card-title">Dashboard</h1>
    <p class="card-text my-4">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    </p>
    <a href="http://localhost/login/?page=logout" class="btn btn-outline-primary px-5">Logout</a>
  </div>
</div>
