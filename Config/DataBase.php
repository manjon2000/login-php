<?php

include_once __DIR__ . '/Settings.php';

class DB {

    public String $host     = DB_SETTINGS['HOST'];
    public String $user     = DB_SETTINGS['USER'];
    public String $password = DB_SETTINGS['PASSWORD'];
    public String $database = DB_SETTINGS['DATABASE'];


    public function connect() {
        $conn = new mysqli($this->host,$this->user,$this->password,$this->database);

        if( $conn->connect_errno ) {
            echo 'La conexion fallo: '. $conn->connect_errno ;
            exit();
        }
        return $conn;
    }

}